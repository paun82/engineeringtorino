package com.engineering.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.engineering.model.User;

public interface UserRepository extends JpaRepository{

}
