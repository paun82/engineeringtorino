package com.engineering.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.engineering.services.interfaces.IUserService;

@Controller
//@RequestMapping("/users")
public class UserController {
	
	@Autowired
	IUserService userService;
	
	
	@RequestMapping("/users")
	public ModelAndView users() {
		
		
		
		//int[] arr = {1,2,58,9,45};
		List all = userService.getAllUsers();
		
		System.out.println(all);
		ModelAndView mv = new ModelAndView("views/users.jsp");
		mv.addObject("users", all);
		return mv;
	}

}
