<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Users page</title>
</head>
<body>
<h1>All users</h1>

<table id="users">
		<thead>
			<tr>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Date of birth</th>
				<th>Username</th>
				<th>Password</th>
				<th>Date of birth</th>
				
			</tr>
		</thead>

		<tbody>
		</tbody>
 				<c:forEach items="${users}" var="item">
		 			<tr> 
						<td>${item.getFirstName()}</td>
						<td>${item.getLastName()}</td>
						<td>${item.getDateOfBirth()}</td>
						<td>${item.getUsername()}</td>
						<td>${item.getPassword()}</td>
		 				<td> 
							<form action="/users/${item.getId()}" method="get">
		 						<input type="submit" value="edit" name="Submit" /> 
		 					</form> 
		 				</td> 
		 			</tr> 
				</c:forEach> 
	</table>

<%-- <h1><%= request.getAttribute("users") %></h1> --%>
<%-- <c:forEach items="${users}" var="item">
	<td><c:out value="${item}" /></td>
</c:forEach>   --%>

</body>
</html>	